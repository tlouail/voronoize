# be aware of

- influence of the points CRS on the Voronoi properties


# code refactoring 

### use cases
- calculate Voronoi diagram properties for a set of cities listed in a file given as an argument

USAGE : 
```
voronize --points file.csv --border file.shp
```

- structure du fichier de points : name, long, lat, pop, country
- option --breaks : nombre de valeurs seuils $P_0$ ; 30 par défaut
- option --null=FALSE : calcul du null model ?
  - argument --rep : nombre de réplications du null model ; par défaut 100
  
- option --cores=1 nombre de coeurs

- sortie : dossier "file.csv-sys.time()"
- un fichier "res-file.csv-sys.time().csv"
- idexp, P0, name, long, lat, pop, country, cell_perimeter, cell_area, cell_pop, cell_n, neighbors_avg_area, neighbors_avg_pop, neighbors_avg_n
- dossier "null"
- un fichier csv par replication "res_nom fichier source + sys.time().csv"
