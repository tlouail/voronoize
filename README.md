# A cautionary note

This repository contains the (raw) source code and data that were used to produce the results and figures presented in the paper ["A dominance tree approach to systems of cities", by Thomas Louail and Marc Barthelemy (2022)](https://arxiv.org/abs/2202.13906). If you hoped to find a convenient, ready-to-load package along with a clear and concise documentation: sorry for the disappointment. So far I did not have time to properly package the code, and in fact even if I had I don't know how to do that. Still I hope you will find here some useful material from which you could work your own implementation of Okabe and Sadahiro's dominance tree, and maybe (hopefully) reproduce or disprove the results presented in the aforementioned paper.

# src

The `src` folder contains the R source code. In particular, the file `spatial-hierarchy-functions.R` includes the `okabe_sadahiro` function that builds the dominance tree given a set of point $x_i$ (`points`), their marks $P_i$ (`values`) and the geometry of the external `envelope` used to crop the Voronoi cells of the points. The important, general bits related to the method itself (not its application to geographical, population data) are in the files `spatial-hierarchy-functions.R` and `core-functions.R`. The rest of the code is mainly application-level files, those related to France are prefixed by `FR`, etc.

# data

The `data` folder contains both the original databases that contain France and US historical city population data (`csv` and `shp` files), and the data structures (`Rdata` files) that represent the dominance tree of these systems of cities, at different dates (`data/US/out/tree` and `data/FR/out/tree` folders). A tree is implemented as a list of lists : each tree level/height is represented by a named list that contains

-   `level` : the height (an `integer` value)
-   `subtrees` : a dataframe that contains as many lines as there are cells/nodes at this height, and whose columns indicate for each node its `id`, its `children`'s ids and all its `descendants`' ids
-   `cells` : a dataframe that contains as many lines as there are cells/nodes at this height in the tree, and whose columns contain the actual information about these cells, including their `id` and `name`, their value/mark (`val`), their basin size (`phi`), their `geometry`

# questions/inquiries

For any question or additonal information request you can reach me (Thomas Louail) at firstname dot lastname at cnrs dot fr
