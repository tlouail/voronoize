\documentclass[10pt,twocolumn,floatfix,superscriptaddress,reprint]{revtex4-1}

\usepackage[paperwidth=210mm,paperheight=297mm,centering,hmargin=2cm,vmargin=2.5cm]{geometry}

\usepackage[pdftex]{graphicx} \usepackage{amssymb,amsmath} \usepackage[utf8]{inputenc}
\usepackage[OT1]{fontenc} \usepackage{hyperref} \usepackage{xcolor}

\begin{document}

\title{The spatial organization of urban hierarchies}
%\title{Evolution of urban spatial hierarchies over several centuries}
% \title{Evolution of spatial hierarchy in urban systems over several centuries}

\author{Thomas Louail} \affiliation{CNRS, UMR 8504 Géographie-cités, Campus
  Condorcet, FR-93322 Aubervilliers}

%\author{Julien Migozzi} \affiliation{CNRS, UMR 8504 Géographie-cités,
% Campus Condorcet, FR-93322 Aubervilliers}

\author{Marc Barthelemy} \affiliation{Institut de Physique Th\'{e}orique, CEA, CNRS-URA 2306,
  FR-91191 Gif-sur-Yvette} \affiliation{CAMS (CNRS/EHESS) 190-198, avenue de France, FR-75244
  Paris Cedex 13}

\newcommand{\marc}[1]{\textcolor{green}{\textbf{@@@ Marc: #1 @@@}}}

\begin{abstract}
  Comparing urban dynamics in different countries and over long periods is a
  difficult task. While the statistical distribution of city sizes has received
  considerable attention, the spatial organisation of these urban hierarchies
  has been much less characterized and compared thanks to well-defined, standard
  methods. We apply a method that was proposed by Okabe and Sadahiro in order to
  compare the evolution over several centuries of the urban hierarchy in
  different countries. We show that 1, 2 and 3
\end{abstract}

\maketitle

The rank-size distribution of city sizes has been the center point of the
understanding of the structure of urban hierarchies, and their evolution through
time. Comparatively, the spatial embedding of these hierarchies has been much
less studied thanks to standard, widely shared methods.

Much work has been devoted to characterize city size distributions and to
propose mechanistic models and fundamental processes that reproduce these
distributions. How to fit the empirical data, which distributions and parameters
are the more relevant, all of this has been extensively discussed. More
specifically, the slope of the rank-size distribution has been broadly used to
summarize the structure of the urban hierarchy: the larger the gradient the more
unequal the city size distribution (see \cite{cottineau:rank-size} for a
quantitative review of published results and datasets).

An important thing to notice is that such comparisons of urban hierarchies
between countries or historical periods are possible because the statistical
distribution is a standardized measure and recognize as the relevant 'object' to
compare these hierarchies. One could argue about how to spatially delineate
cities, which data sources are the best, or which statistical method one should
apply to fit the data. But there is no debate about whether or not the histogram
of population sizes is the good measure to quickly grasp the structure of the
urban hierarchy.

How these urban hierarchies are organized in space has been,
comparatively, a question that has been much less studied in a
systematic and comparative way. We argue that the reason is that there
is no standard and shared object such as the histogram to capture the
spatial organization of a hierarchy. An histogram contains a lot of
quantitative information. To the opposite, the spatial indicators
generally applied are either average values (such as the mean distance
to the closest city \cite{bretagnolle-2006}), or standard spatial
statistics indicators (such as Ripleys'K or Moran's I) that tell us to
which extent the point pattern is close to a well-defined spatial
configuration --- e.g. complete spatial randomness, regular lattice or
clustered. Indicators compress the information and thus provide an
incomplete complete description, at different scales, of the spatial
structure of the urban hierarchy.

While theoretical models of spatial organization, such as
Christaller's or Losch's, have been widely influential for the entire
discipline of regional science, few tools and models are available
when one wants to capture both the statistical distribution of city
sizes and their spatial distribution.

At its core, a spatial hierarchy is a combination of two distributions: (i) the
distribution of city population sizes $P_i$, and (ii) the spatial distribution
of these cities locations $x_i$. Most attemps to characterize and to reproduce
both aspects simultaneously have mostly relied upon ad-hoc simulation models
(\cite{simpop}). We argue that these models fail to convince and become
standards because they are indeed too sophisticated. They may contain many
ingredients that interact with each other, and the resulting dynamics are hard
to understand. The results of such simulations are consequently hard to
generalize and of limited interest.

Okabe and Sadahiro proposed a simple, parameter-free method to capture spatial
hierarchies. It is built upon the Voronoi diagram and tree data structure. In
the following we apply their method to build trees that capture urban hierarchy
in France from 1870 to nowadays.

% \paragraph{Why it is relevant to compare urban systems in Europe,
% China and ex-USSR} These particular countries form a system
% because: \begin{itemize} \item they share or shared a common
%   political history (exUSSR, EU) or governance (EU, China) \item
%   through centuries they have developped particular interactions
%   (migrations, commercial, cultural) due to their relative
%   locations \item urbanization in these regions took place at
%   different historical periods and at different speeds. China
%   currently experiences unprecetended urbanization; to the opposite
%   Europe and the ex-USSR have old and mature urban systems whose
%   hierarchy is very stable.  \end{itemize}

\section*{Results}

The following results were produced thanks to open data and open source software
that are available at
\href{https://gitlab.huma-num.fr/tlouail/voronoize}{https://gitlab.huma-num.fr/tlouail/voronoize}. The
source code relies on various R statistical software libraries, the most
decisive being \emph{sf}\cite{} and \emph{lwgeom}\cite{} to manipulate
geographical data and perform geometrical operations.

\subsection*{Geometrical properties of urban tesselations at different
  scales}

We first use the Voronoi tesselation to highlight some of the geometrical
properties of the spatial point pattern formed by French municipalities, at
different population scales. Namely we consider population thresholds $P_0$ in
the range $10^3--10^6$ (the largest municipality in France is Paris with
$P \approx 2.10^6$). For a given $P_0$ we filter cities such as $P_i \geq P_0$
and build the corresponding Voronoi tesselation from their locations $x_i$. The
number of cities such as $P_i \geq P_0$ is plotted on figure
\ref{fig:N_P0}. From the $P_0$ value we calculate the corresponding density
$\rho$ of the point pattern, in order to compare some of the geometrical
properties observed with the complete spatial randomness case (spatial
Poisson). To control for border effects due to the peculiarity of the frontiers
geometry, we also simulate a spatial point process of equal density inside the
country borders, and then consider the bulk of the resulting tesselation,
cropped with the country's geometrical envelope.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_N_vs_P0.pdf}
  \caption{\label{fig:N_P0} Number of French municipalities such as $P\geq P_0$.}
\end{figure}

\paragraph{Perimeter of the Voronoi cells}

In the spatial Poisson-Voronoi case the average perimeter of Voronoi
cells is given by
$$
\langle p\rangle=\frac{4}{\sqrt{\rho}}
$$ where $\rho$ is the density of the point pattern. These values are
reported in red on Figure \ref{fig:p_P0} for different density values,
along with the average perimeter of the Voronoi cells built from the
city locations. In the range of values considered here the average
perimeter of city cells is smaller than for the spatial Poisson
case. The larger the density (greater number of cities, smaller $P_0$
values), the more the average perimeter is close to the value obtained
in the Poisson case. For smaller values of $P_0$ however (only large
cities are considered), the pattern seems to

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_avg_perimeter_vs_density.pdf}
  \caption{\label{fig:p_P0} Average perimeter $<p>$ of the Voronoi
    cells built from the municipalities centroids. Value are plotted
    against the density $\rho$ of the point pattern, for the spatial
    Poisson case (red) ; for the data including country borders' cells
    (green) and in the bulk (blue ; for this latter case for there is
    no data for the smallest density values (highest $P_0$) because
    all cells touch the country border.}
\end{figure}


Another interesting quantity is the statistical distribution of the
cells areas. In the complete spatial randomness (CSR) case we
have $\sigma^2(a_i) = 0.28/\rho^2$. On Figure \ref{fig:var-area} we
plot the variance $\sigma^2(a_i)$ of the cells area $a_i$ as a
function of the density $\rho$ of the point pattern, for the
theoretical Poisson case and for the observed data.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_var_a_vs_rho.pdf}
  \caption{\label{fig:var-area} Variance of the Voronoi cells area
    vs. the density of the point pattern.}
\end{figure}


\paragraph{Shapes}

Looking at the average number of sides of cells $\langle n\rangle$
(averaged over the bulk and after removing the cells touching the
country borders), we wonder if there is a population size threshold
above which the spatial distribution of cities would diverge from the
average value measured in the CSR case). We note that
$\langle n\rangle$ corresponds to the average degree of the
corresponding Delaunay graph [A paper providing results on
this](https://arxiv.org/pdf/cond-mat/0605136.pdf)

\begin{figure}[htb]
  \centering
  \includegraphics[width=\linewidth]{figs/FR_avg_n_vs_P0.pdf}
  \caption{$<n>$ vs $P_0$}
  \label{fig:n_vs_P0}
\end{figure}


\subsection*{Aboav-Weaire law and Lewis law}

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_a_vs_n_facetsP0.pdf}
  \caption{\label{fig:N_P0} $\overline{a(n)}=f(n)$ for different population threshold.}
\end{figure*}

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_Aboav.pdf}
  \caption{\label{fig:Aboav}$nm(n)=f(n)$.}
\end{figure*}


% \begin{table}
% \caption{\label{tab:lewi-linear-fit}Lewis law -- linear fit $a=kn + b$ for different $P_0$ values}
% \centering
% \begin{tabular}[t]{l|r|r|r|r}
% \hline
%   & P0 & k & b & $R^2$\\
% \hline
% \end{tabular}
% \end{table}


\subsection*{Spatial dominance tree}

Okabe and Sadahiro \cite{okabe:1998} proposed a simple and
well-defined method to capture spatial hierarchy in a settlement
system (see the Methods section). They applied this method to discuss
the geometrical properties that Christaller attributed to organized
urban systems, and they showed indeed that several of these properties
are observed in the standard random case, that is for a spatial
Poisson point process.

Here we apply their method and build the spatial dominance tree of
France at different points in history, between 1876 and 2017. This
gives us a well-defined data structure useful to investigate the
evolution of the spatial properties of the French urban hierarchy.

We compare the values measured in the data with those obtained for a
complete random case (spatial poisson process) and to a spatially
constrained null model (the populations are reshuffled among the
existing seeds).

We do that for two different countries for which we analyze the
spatial distribution of municipality population sizes, which are
publicly available on national open data portals: Metropolitan France
and xxx. The spatial organization of local hierarchies around cities
of different sizes is also investigated.


\subsubsection*{Global scale}
\label{sec:spatial-hierarchy-global}

\paragraph{tree structure -- depth and balance}

\paragraph{cell population}

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_Gini_vs_k_years.pdf}
  \caption{\label{fig:Gini-vs-k}The Gini value of the cell population
    sizes $(\phi_i)$ and areas $(a_i)$ distributions, at different
    levels in the spatial dominance tree for France, at different
    dates.}
\end{figure*}


\paragraph{cell area}

\subsubsection*{Local scale}

\section*{Methods}

\subsection*{Data}
Available on INSEE (historical municipality populations) and IGN
(geometries) opendata portals.

\subsection*{Okabe-Sadahiro spatial dominance tree}

see \cite{okabe:1998}.

Repeat:
\begin{enumerate}
\item build the Voronoi tesselation from the municipalities centroids
\item determine the population local maxima, i.e. the cells which have no neighbor cell whose
  population is larger
\item build the new Voronoi tesselation from the local maxima centroids
\end{enumerate}
until there's only one cell left. This cell is the root of the tree
(by construction it is necessarily the city with the largest
population size)

\newpage

\section*{Supplementary Figures}

\clearpage

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_p_vs_P.pdf}
  \caption{\label{fig:p_vs_P} $a_i$ vs $P_i$ for different population thresholds $P_0$}
\end{figure*}


\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{figs/FR_a_vs_P.pdf}
  \caption{\label{fig:a_vs_P} }
\end{figure*}


% \begin{thebibliography}{}
%   % \bibitem
% \end{thebibliography}

\end{document}
