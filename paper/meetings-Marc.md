# notes réunion Marc vendredi 6 décembre 2019

3 grandeurs différentes pour laquelle on peut calculer le Gini à chaque niveau de l'arbre :
- nombre d'enfants de chaque noeud > changer la terminologie dans la figure
- nombre de feuilles du sous-arbre (noeuds terminaux)
- taille du sous-arbre (nombre total de noeuds)

# échelle locale
- différences des arbres de Paris - Lyon - Marseille 
- dans un périmètre raisonnable, construire l'arbre et l'afficher
- Paris c'est l'ile-de-france
- Marseille et Lyon on connaît moins bien 
- différences dans la structure de l'arbre
- Catifori, processus additifs ou multiplicatifs
- nombre de villes directement reliées à Paris-Centre vs. le reste

absorbtion pas concentrique, pas isotrope, semble plus régulier dans le cas de certaines villes que dans d'autres
une carte peut dire pas mal de choses sur l'organisation locale 

comparer avec la carte de l'absorbtion de la ville dans un modèle null contraint 
(par exemple tout mélanger sauf Paris)
