source("src/core-functions.R")
source("src/spatial-hierarchy-functions.R")
library(tidyr)
library(parallel)
library(pbmcapply)

n <- 10^4
bbox <- st_sfc(st_polygon(list(rbind(c(-1,-1), c(-1,1), c(1,1), c(1,-1), c(-1,-1)))))
#PTS <- st_sample(x=bbox, size=n) %>% st_geometry()
g <- 1
x_min <- 1000
VALS <- poweRlaw::rpldis(n=n, xmin=x_min, alpha=1+g)
MC.CORES <- 4

trees <- pbmcmapply(fixedPtsPowerLaw, 
                        coupling=rep("sierpinski",200),
                        MoreArgs=list(pts=st_sample(x=bbox, size=n) %>% st_geometry(),
                                      val=VALS,
                                      bbox=bbox), 
                        SIMPLIFY=F,
                        mc.cores=MC.CORES)
write_rds(trees, "data/geo/FR/out/tree/toy/trees_toy_model_Sierpinski_diff_PP.Rds")

h_P_trees <- do.call(rbind, lapply(seq_along(trees),
                                   function(x) get_levels_p_phi(trees[[x]]) %>%
                                     mutate(id_rep=x)))
write_csv(h_P_trees, "data/geo/FR/out/tree/toy/h_P_trees_toy_model_Sierpinski_diff_PP.csv")

