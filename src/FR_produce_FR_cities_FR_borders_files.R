library(tidyverse)
library(sf)

communes <- st_read("./data/geo/FR/in/FR_communes.shp") 

communes %>% 
  st_centroid() %>% 
  st_set_crs(2154) %>% 
  rename(insee_id=INSEE_COM,
         name=NOM_COM,
         P=POPULATION, 
         dept=INSEE_DEP) %>% 
  arrange(desc(P)) %>% 
  mutate(id=1:n()) %>% 
  select(id, insee_id, name, dept, P) %>% 
  st_write("./data/geo/FR/in/FR_cities.shp", delete_dsn = TRUE)

communes %>% 
  st_geometry() %>% 
  st_union() %>%
  st_cast("POLYGON") %>%
  st_set_crs(2154) %>% 
  st_write("./data/geo/FR/in/FR_borders.shp", delete_dsn = TRUE)

