source("src/core-functions.R")
source("src/spatial-hierarchy-functions.R")
library(ggplot2)
library(latex2exp)
library(ggstar)

firstup <- function(x) {
  substr(x, 1, 1) <- toupper(substr(x, 1, 1))
  x
}

## FR
FR_h_r_phi <- data.frame()
for (f in list.files("data/geo/FR/out/tree", pattern = "*.Rds",full.names = T)){
  print(f)
  tree <- read_rds(f)
  y <- unlist(regmatches(f, gregexpr("\\d{4}", f))) %>% as.numeric() 
  FR_h_r_phi <- rbind(FR_h_r_phi, get_levels_p_phi(tree) %>% mutate(year=y))
}

FR_types <- FR_h_r_phi %>% 
  arrange(id,name,year) %>% 
  group_by(id,name) %>% 
  summarize(is_stable=all(diff(level)==0),
            delta=sum(diff(level)), 
            delta_abs=sum(abs(diff(level)))) %>% 
  mutate(type=ifelse(is_stable==T,
                     "s",
                     ifelse(is_stable==F & delta==0,
                            "sf", 
                            ifelse(is_stable==F & delta<0,
                                   "d",
                                   "u"))))

FR_h_r_phi <- FR_h_r_phi %>% 
  rename(h=level) %>% 
  left_join(FR_types) %>% 
  group_by(year) %>% 
  mutate(hnorm=ifelse(h==max(h),
                      "H",
                      ifelse(h==(max(h)-1),
                             "H-1",
                             ifelse(h==(max(h)-2),
                                    "H-2",
                                    ifelse(h==max(h)-3,
                                           "H-3",
                                           ifelse(h==max(h)-4,
                                                  "H-4",
                                                  ifelse(h==max(h)-5,
                                                         "H-5",
                                                         ifelse(h==max(h)-6,
                                                                "H-6",
                                                                "other"))))))))


FR_plot_cities <- FR_h_r_phi %>% 
  group_by(year) %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n()) %>% 
  filter(year==2017 & rank <=20) %>% 
  pull(name) %>% 
  unique() %>% 
  tolower() %>% 
  firstup()

cbbPalette <- c("#56B4E9", "#0072B2", "#009E73", "#D55E00")

FR_h_r_phi %>%
  mutate(hnorm=factor(hnorm, 
                      levels=c("H", "H-1", "H-2", 
                               "H-3", "H-4", "H-5", "H-6"))) %>% 
  mutate(name=firstup(tolower(name))) %>% 
  mutate(name=factor(name,
                     levels=rev(plot_cities))) %>% 
  mutate(type=factor(type, levels=c("s", "sf", "u", "d"))) %>% 
  group_by(year) %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n()) %>% 
  arrange(year, rank) %>% 
  filter(name %in% plot_cities) %>% 
  ggplot(aes(x=name,y=hnorm,colour=type,alpha=year))+
  geom_jitter(size=1.6,width = 0.25,height=0.25,stroke=0.2)+
  geom_star(data=FR_h_r_phi %>% 
              mutate(hnorm=factor(hnorm, 
                                  levels=c("H", "H-1", "H-2", 
                                           "H-3", "H-4", "H-5", "H-6"))) %>% 
              mutate(name=firstup(tolower(name))) %>% 
              mutate(name=factor(name,
                                 levels=rev(FR_plot_cities))) %>% 
              mutate(type=factor(type, levels=c("s", "sf", "u", "d"))) %>% 
              filter(name %in% FR_plot_cities & year==2017) %>% 
              group_by(year) %>% 
              arrange(desc(P)) %>% 
              mutate(rank=1:n()) %>% 
              arrange(year, rank), aes(fill=type),colour="black", starshape=1,size=1.8)+
  coord_flip()+
  scale_color_manual(values=cbbPalette,guide=F)+
  scale_fill_manual(values=cbbPalette,guide=F)+
  scale_alpha_continuous(guide=F)+
  scale_y_discrete("")+
  scale_x_discrete("")+
  theme_bw()+
  theme(axis.text.x = element_text(size=8,
                                   face="bold"),
        axis.text.y = element_text(size=8,
                                   face="bold"), 
        axis.title=element_blank(),
        legend.position="bottom", 
        legend.direction = "horizontal")+
  ggsave("figs/FR_participtations.pdf",
         width=3.4,
         height = 3.4,
         units="in")


## US

US_h_r_phi <- data.frame()
for (f in list.files("data/geo/US/out/tree", pattern = "*.Rds",full.names = T)){
  print(f)
  tree <- read_rds(f)
  y <- unlist(regmatches(f, gregexpr("\\d{4}", f))) %>% as.numeric() 
  US_h_r_phi <- rbind(US_h_r_phi, get_levels_p_phi(tree) %>% mutate(year=y))
}

US_types <- US_h_r_phi %>% 
  arrange(id,name,year) %>% 
  group_by(id,name) %>% 
  summarize(is_stable=all(diff(level)==0),
            delta=sum(diff(level)), 
            delta_abs=sum(abs(diff(level)))) %>% 
  mutate(type=ifelse(is_stable==T,
                     "s",
                     ifelse(is_stable==F & delta==0,
                            "sf", 
                            ifelse(is_stable==F & delta<0,
                                   "d",
                                   "u"))))

US_h_r_phi <- US_h_r_phi %>% 
  rename(h=level) %>% 
  left_join(US_types) %>% 
  group_by(year) %>% 
  mutate(hnorm=ifelse(h==max(h),
                      "H",
                      ifelse(h==(max(h)-1),
                             "H-1",
                             ifelse(h==(max(h)-2),
                                    "H-2",
                                    ifelse(h==max(h)-3,
                                           "H-3",
                                           ifelse(h==max(h)-4,
                                                  "H-4",
                                                  ifelse(h==max(h)-5,
                                                         "H-5",
                                                         ifelse(h==max(h)-6,
                                                                "H-6",
                                                                "other"))))))))

US_plot_cities <- US_h_r_phi %>% 
  group_by(year) %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n()) %>% 
  filter(year==2010 & rank <=20) %>% 
  pull(name) %>% 
  unique()

US_plot_ids <- US_h_r_phi %>% 
  group_by(year) %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n()) %>% 
  filter(year==2010 & rank <=20) %>% 
  unique() %>% 
  pull(id)

cbbPalette <- c("#56B4E9", "#0072B2", "#009E73", "#D55E00")

US_h_r_phi %>%
  filter(id %in% US_plot_ids) %>% 
  mutate(hnorm=factor(hnorm, 
                      levels=c("H", "H-1", "H-2", 
                               "H-3", "H-4", "H-5", "H-6"))) %>% 
  #mutate(name=firstup(tolower(name))) %>% 
  mutate(name=factor(name,
                     levels=rev(US_plot_cities))) %>% 
  mutate(type=factor(type, levels=c("s", "sf", "u", "d"))) %>% 
  group_by(year) %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n()) %>% 
  arrange(year, rank) %>% 
  ggplot(aes(x=name,y=hnorm,colour=type,alpha=year))+
  geom_jitter(size=1.6,width=0.25,height=0.25,stroke=.2)+
  geom_star(data=US_h_r_phi %>% 
              filter(id %in% US_plot_ids & year==2010) %>% 
              mutate(hnorm=factor(hnorm, 
                                  levels=c("H", "H-1", "H-2", 
                                           "H-3", "H-4", "H-5", "H-6"))) %>% 
              mutate(name=factor(name,
                                 levels=rev(US_plot_cities))) %>% 
              mutate(type=factor(type, levels=c("s", "sf", "u", "d"))) %>% 
              group_by(year) %>% 
              arrange(desc(P)) %>% 
              mutate(rank=1:n()) %>% 
              arrange(year, rank), aes(fill=type),colour="black", starshape=1,size=1.8)+
  coord_flip()+
  scale_color_manual(values=cbbPalette, guide=F)+
  scale_fill_manual(values=cbbPalette, guide=F)+
  scale_alpha_continuous(guide=F)+
  scale_y_discrete("")+
  scale_x_discrete("")+
  theme_bw()+
  theme(axis.text.x = element_text(size=8,
                                   face="bold"),
        axis.text.y = element_text(size=8,
                                   face="bold"), 
        axis.title = element_blank())+
  ggsave("figs/US_participtations.pdf",
         width=3.4,
         height = 3.4,
         units="in")


# Check for both countries if hmax is constant or not
# hmax=6 whatever the year, and we do not need to renormalize the h values form a date to another
# FR_h_r_phi %>% 
#   rename(h=level) %>% 
#   group_by(year) %>% 
#   summarize(H=max(h))

# hmax is not constant so we have to renormalize the h values to make them comparable between dates
# US_h_r_phi %>% 
#   rename(h=level) %>% 
#   group_by(year) %>% 
#   summarize(H=max(h))



# FR_h_r_phi %>% 
#   mutate(hnorm=factor(hnorm, levels=c("H", "H-1", "H-2", 
#                                       "H-3", "H-4", "H-5", "H-6"))) %>% 
#   group_by(id,name,hnorm) %>% 
#   summarize(n_years=n()) %>% 
#   mutate(prop=100*n_years/NYEARS) %>% 
#   select(hnorm,id,name,n_years,prop) %>% 
#   arrange(hnorm,desc(prop))

# US_h_r_phi %>% 
#   mutate(hnorm=factor(hnorm, levels=c("H", "H-1", "H-2", 
#                                       "H-3", "H-4", "H-5", "H-6"))) %>% 
#   group_by(id,name,hnorm) %>% 
#   summarize(n_years=n()) %>% 
#   mutate(prop=100*n_years/NYEARS) %>% 
#   select(hnorm,id,name,n_years,prop) %>% 
#   arrange(hnorm,desc(prop))

# h_sizes_sets <- FR_h_r_phi %>%
#   mutate(hnorm=factor(hnorm, levels=c("H", "H-1", "H-2", "H-3", 
#                                       "H-4", "H-5", "H-6"))) %>% 
#   group_by(hnorm) %>% 
#   summarize(N=n_distinct(id)) %>% 
#   mutate(prop=100*N/max(NCITIES_FR$N)) %>% 
#   mutate(country="FR 1876-2010")

# h_sizes_2017 <- FR_h_r_phi %>%
#   rename(h=level) %>% 
#   filter(year==2017) %>% 
#   group_by(h) %>% 
#   summarize(N_2017=n_distinct(id)) %>% 
#   mutate(prop_2017=100*N_2017/max(NCITIES$N))
# 
# h_sizes_sets %>% 
#   left_join(h_sizes_2017) %>% 
#   mutate(mix_ratio=(N-N_2017)/N_2017)

# h_sizes_sets %>% 
#   ggplot(aes(x=hnorm,y=N,colour=country))+
#   geom_point()+
#   scale_y_log10(TeX("$n_{t_0->t_f}(h)$"))+
#   theme_bw()+
#   theme(legend.position = c(1,0),
#         legend.justification = c(1.1,-0.1))+
#   ggsave("figs/nc_vs_h_FR_US.pdf",
#          width=3.4,
#          height = 3.4,
#          units="in")

## 

# FR_h_r_phi %>%
#   mutate(hnorm=factor(hnorm, levels=c("H", "H-1", "H-2", 
#                                       "H-3", "H-4", "H-5", "H-6"))) %>% 
#   group_by(hnorm,id,name) %>% 
#   summarize(n=n_distinct(year)) %>% 
#   filter(hnorm%in%c("H", "H-1", "H-2")) %>% 
#   mutate(country="FR 1876-2010") %>% 
#   ggplot(aes(x=hnorm,y=100*n/21,colour=name,label=name))+
#   #geom_jitter(alpha=.5)+
#   geom_text(size=2)+
#   scale_y_continuous("Participation rate (% dates)")+
#   scale_color_discrete(guide=F)+
#   theme_bw() + 
#   ggsave("figs/participation_rates.pdf",
#          width=3.4,
#          height=3.4,
#          units="in")