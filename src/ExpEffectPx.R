source("src/core-functions.R")
source("src/spatial-hierarchy-functions.R")
library(parallel)
library(poweRlaw)
library(ineq)
library(pbmcapply)

bbox <- st_sfc(st_polygon(list(rbind(c(-1,-1), c(-1,1), c(1,1), c(1,-1), c(-1,-1)))))
nrep <- 200 # number of simulations per set of parameter values 
x_min <- 1000 # min population of the smallest point
gammas <- 1#:3
P0 <- x_min
eps <- .05
sizes <- round(sort(exp(seq(log(10^2),log(10^5),length.out = 30))))
res_all <- data.frame()

for (n in sizes){
  PTS <- st_sample(x=bbox, size=n) %>% 
    st_geometry()
  for (g in gammas){
    print(c(n,g))
    treesPLC
    treesPL <- pbmcmapply(fixedPtsPowerLaw, 
                          gamma=rep(g, nrep),
                          MoreArgs=list(pts=PTS, 
                                        bbox=bbox, 
                                        x_min=x_min), 
                          mc.cores=10)
    #write_rds(treesPL, paste(n,g,"trees.rds", sep = "_"))
    # chis <- unlist(lapply(seq_along(treesPL), function(x) lapply(seq_along(treesPL), function(y) {
    #   if(x!=y) 
    #     res <- chi(treesPL[[x]],treesPL[[y]])
    #   else
    #     res <- NA
    #   return(res)
    # })))
    res_all <- rbind(res_all,data.frame(n=n,
                                        avg.g=mean(lengths(treesPL), na.rm=T),
                                        sd.g=sd(lengths(treesPL), na.rm=T),
                                        logn_log_6=log(n)/log(6)))
    
    # res_all <- rbind(res_all, data.frame(n=n,
    #                                      type="f(P)~1/P^{1+\\gamma}",
    #                                      param=g,
    #                                      avg.chi=mean(chis, na.rm=T),
    #                                      sd.chi=sd(chis, na.rm=T)))
  }
  # print(c(n,eps))
  # treesP0Eps <- pbmcmapply(fixedPtsP0Epsilon, 
  #                          epsilon=rep(eps,nrep),
  #                          MoreArgs=list(pts=PTS, 
  #                                        bbox=bbox, 
  #                                        P0=P0), 
  #                          mc.cores=8)
  # write_rds(treesP0Eps, paste(n,eps,"trees.rds", sep = "_"))
  # chis <- unlist(lapply(seq_along(treesP0Eps), function(x) lapply(seq_along(treesP0Eps), function(y) {
  #   if(x!=y) 
  #     res <- chi(treesP0Eps[[x]],treesP0Eps[[y]])
  #   else
  #     res <- NA
  #   return(res)
  #})))
  # res_all <- rbind(res_all, data.frame(n=n, 
  #                                      type="f(P)~P0+\\epsilon",
  #                                      param=eps,
  #                                      avg.chi=mean(chis, na.rm=T),
  #                                      sd.chi=sd(chis, na.rm=T)))
}

write_csv(res_all, "data/avg.g_vs_n.csv")

library(ggplot2)
library(latex2exp)
require(scales)

model <- lm(avg.g~log(n), data = res_all)
summary(model)

# <g>=f(n)
res_all %>% 
  ggplot(aes(x=n, y=avg.g))+
  geom_point()+
  scale_x_log10()+
  scale_y_continuous(TeX("$<g>$"))+
  theme_bw()+
  stat_function(fun = function(x) 1 + log(x)/log(6))+
  stat_function(fun = function(x) 1.25 + log(x)/log(6), col="blue")+
  stat_function(fun = function(x) 1.66 + 0.5*log(x), col="red")+
  ggsave("figs/avg_g_vs_n.pdf", 
         height=3.4, 
         width=3.4, 
         units="in")


res_all %>% 
  mutate(param=factor(param)) %>% 
  ggplot(aes(x=n, y=avg.chi, colour=type, linetype=param))+
  geom_line()+
  geom_point(shape=21,fill="white")+
  scale_x_log10()+
  scale_y_log10(TeX("$\\bar{\\chi}$"))+
  scale_linetype_discrete(name="",
                          labels=append(lapply("", TeX),
                                        lapply(sprintf('$\\gamma = %d$', c(1:3)), TeX)))+
  scale_colour_discrete(name="",
                        labels=append(lapply("$f(P) \\propto 1/P^{1+\\gamma}$", TeX),
                                      lapply("$f(P) \\propto P_0+\\epsilon$", TeX)))+
  theme_bw()+
  theme(legend.position = c(0,1),
        legend.justification = c(-0.1,1.1))+
  ggsave("expEffectsPx.pdf", 
         height=3.4, 
         width=3.4,
         units="in")
