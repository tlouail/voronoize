source("src/core-functions.R")
source("src/spatial-hierarchy-functions.R")
library(tidyr)
library(ineq)
library(ggplot2)
library(scales)
library(grid)
library(latex2exp)
library(parallel)
library(pbmcapply)

n <- 10^4
bbox <- st_sfc(st_polygon(list(rbind(c(-1,-1), c(-1,1), c(1,1), c(1,-1), c(-1,-1)))))
PTS <- st_sample(x=bbox, size=n) %>% st_geometry()
g <- 1
x_min <- 1000
VALS <- poweRlaw::rpldis(n=length(PTS), xmin=x_min, alpha=1+g)
MC.CORES <- 22

treesb <- pbmcmapply(fixedPtsPowerLaw, 
                     coupling=rep("b",200),
                     MoreArgs=list(pts=PTS,
                                   val=VALS,
                                   bbox=bbox), 
                     SIMPLIFY=F,
                     mc.cores=MC.CORES)
# tree_b <- fixedPtsPowerLaw(pts=PTS,val=VALS,bbox=bbox,coupling="b")

plot_data_b <- do.call(rbind, lapply(seq_along(treesb), 
                                     function(x) get_levels_p_phi(treesb[[x]]) %>% 
                                       mutate(id_rep=x,
                                              coupling="b")))




# tree_c <- fixedPtsPowerLaw(pts=PTS,val=VALS,bbox=bbox,coupling="c")

L_values <- c(exp(seq(log(1/sqrt(n/st_area(bbox))), log(sqrt(st_area(bbox))), length.out = 6)))
L_values <- round(L_values, 2)
L_values <- rep(L_values,100)
treesc <- pbmcmapply(fixedPtsPowerLaw, 
                     coupling=rep("c",length(L_values)),
                     L=L_values,
                     MoreArgs=list(pts=PTS,
                                   val=VALS,
                                   bbox=bbox), 
                     SIMPLIFY=F,
                     mc.cores=MC.CORES)

plot_data_c <- do.call(rbind, lapply(seq_along(treesc), 
                                     function(x) tibble(L=L_values[x],
                                                        get_levels_p_phi(treesc[[x]])) %>% 
                                       mutate(id_rep=x, 
                                              coupling="c")))

plot_data <- rbind(plot_data_c,
                   plot_data_b %>% 
                     mutate(L= "Inf") %>% 
                     select(L,id,level,P,phi,name,id_rep,coupling))

ma <- function(x, n = 5){
  stats::filter(x, rep(1 / n, n), sides = 1)
}

plot_data %>% 
  mutate(level=as.integer(level)) %>% 
  group_by(id,P,coupling,L) %>% 
  summarize(avg.level=mean(level, na.rm=T), 
            min.level=min(level, na.rm = T), 
            max.level=max(level, na.rm=T)) %>% 
  # ungroup() %>% 
  # arrange(coupling, L,P) %>% 
  # group_by(coupling,L) %>% 
  # mutate(m.avg.level=ma(avg.level, n=20)) %>% 
  ggplot(aes(x=P, y=avg.level, ymin=min.level, ymax=max.level, colour=L, fill=L))+
  geom_line()+
  geom_point(shape=21,size=.1,stroke=.2)+
  #geom_linerange()+
  scale_x_log10(labels=comma)+
  scale_y_continuous("<h(P)>")+
  theme_bw()+
  theme(legend.position = c(0,1), 
        legend.justification = c(-.1,1.05),
        legend.background = element_rect(color="black"))+
  ggsave("figs/avg_h_vs_P_toy_model.png", 
         height=3.4, 
         width=3.4, 
         units="in", 
         dpi=600)


