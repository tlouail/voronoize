source("src/core-functions.R")
source("src/spatial-hierarchy-functions.R")
library(parallel)
library(pbmcapply)
library(tidyr)
library(ggplot2)
library(scales)
library(grid)
library(latex2exp)

## unites urbaines

tree_uu <- read_rds("data/geo/FR/out/tree/FR_tree_unites_urbaines.Rds")
uu_comp_com <- readxl::read_excel("data/geo/FR/in/UU2020_au_01-01-2021.xlsx", 
                                  sheet="Composition_communale",
                                  skip=5)

tmp <- get_levels_p_phi(tree_uu) %>% 
  ungroup() %>% 
  arrange(desc(P)) %>% 
  rename(h_uu=level) %>% 
  mutate(r_uu=1:n())

r_breaks <- round(exp(seq(log(1),
                          log(max(tmp$r_uu)),
                          length.out=length(unique(tmp$h_uu))+1)))

tmp <- tmp %>% 
  mutate(r_uu_cat=findInterval(r_uu, r_breaks))

h_mun_uu <- uu_comp_com %>% 
  inner_join(tmp, by=c("UU2020"="id")) %>% 
  select(CODGEO,LIBGEO,h_uu,r_uu,r_uu_cat)


## aires attraction des villes

tree_aav <- read_rds("data/geo/FR/out/tree/FR_tree_aires_attraction_villes.Rds")
aav_comp_com <- st_read("data/geo/FR/in/AAV20_compcom_geo2021.shp") %>% 
  st_set_geometry(NULL)

tmp <- get_levels_p_phi(tree_aav) %>% 
  ungroup() %>% 
  arrange(desc(P)) %>% 
  rename(h_aav=level) %>% 
  mutate(r_aav=1:n())

r_breaks <- round(exp(seq(log(1),
                          log(max(tmp$r_aav)),
                          length.out=length(unique(tmp$h_aav))+1)))

tmp <- tmp %>% 
  mutate(r_aav_cat=findInterval(r_aav, r_breaks))

h_mun_aav <- aav_comp_com %>% 
  inner_join(tmp, by=c("AAV2020"="id")) %>% 
  select(CODGEO,LIBGEO,h_aav,r_aav,r_aav_cat)

tmp <-h_mun_uu %>% 
  inner_join(h_mun_aav)

cor(tmp$h_uu, tmp$h_aav, method = "kendall")
cor(tmp$r_uu, tmp$r_aav, method = "kendall")
cor(tmp$r_uu_cat, tmp$r_aav_cat, method = "kendall")

######

tree_coremun <- read_rds("data/geo/FR/out/tree/FR_tree_core_municipalities_urban_areas_2017.Rds")

l <- list(tree_ua, tree_coremun)

res <- lapply(seq_along(l), 
              function(x) data.frame(level=0:(length(l[[x]])-1),
                                     Gini=unlist(Gini_subtrees_sizes(l[[x]])),
                                     var="#descendants",
                                     type=x)) %>% do.call(rbind, .) %>% 
  rbind(
    lapply(seq_along(l), 
           function(x) data.frame(level=0:(length(l[[x]])-1),
                                  Gini=unlist(Gini_children_number(l[[x]])),
                                  var="#children",
                                  type=x)) %>% do.call(rbind, .)) %>% 
  rbind(
    lapply(seq_along(l), 
           function(x) data.frame(level=0:(length(l[[x]])-1),
                                  Gini=unlist(lapply(l[[x]], function(y) Gini(y$cells$phi)*
                                                       (length(y$cells$phi)/(length(y$cells$phi)-1)))),  # normalize Gini
                                  var="phi",
                                  type=x)) %>% do.call(rbind, .)) %>% 
  rbind(
    lapply(seq_along(l), 
           function(x) data.frame(level=0:(length(l[[x]])-1),
                                  Gini=unlist(lapply(l[[x]], function(y) Gini(st_area(y$cells))*
                                                       (length(st_area(y$cells))/(length(st_area(y$cells))-1)))),   # normalize Gini
                                  var="area",
                                  type=x)) %>% do.call(rbind, .)) %>% 
  mutate(type=ifelse(type==1, "UA", "CoreMunUA"))


res

### Compare individual city levels in both trees

df <- get_levels_p_phi(tree_ua) %>%
  ungroup() %>% 
  select(id,phi,level) %>% 
  rename(phi_ua=phi,
         level_ua=level)

df2 <- get_levels_p_phi(tree_coremun) %>% 
  ungroup() %>% 
  select(id,phi,level) %>% 
  rename(phi_mun=phi,
         level_mun=level)

## scatterplot h_mun vs. h_ua 
left_join(df, df2) %>% 
  mutate(label=ifelse((level_ua>=2 | level_mun >=2) & (!level_ua==level_mun), 
                      id, 
                      NA)) %>% 
  mutate(delta=abs(level_ua-level_mun)) %>% 
  left_join(get_levels_p_phi(tree_coremun) %>% 
              select(id, name), 
            by=c("label"="id")) %>% 
  ggplot(aes(x=level_mun, y=level_ua, label=name))+
  geom_abline(slope = 1, linetype="dashed")+
  geom_jitter(alpha=.1, height=.02, width=.02)+
  geom_text(size=2, position=position_jitter(width=.2, height=.2))+
  scale_x_continuous(TeX("$h_{mun}$"))+
  scale_y_continuous(TeX("$h_{ua}$"))+
  theme_bw()
ggsave("figs/FR_hua_vs_hmun.pdf", 
       width=3.4, 
       height=3.4, 
       units="in")



## kendall tau between h_ua and h_mun

df <- get_levels_p_phi(tree_ua) %>%
  ungroup() %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n()) %>% 
  select(id,phi,level,rank) %>% 
  rename(phi_ua=phi,
         level_ua=level,
         rank_ua=rank)
  
df2 <- get_levels_p_phi(tree_coremun) %>% 
  ungroup() %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n()) %>% 
  select(id,phi,level,rank) %>% 
  rename(phi_mun=phi,
         level_mun=level,
         rank_mun=rank)

levels_ua <- left_join(df, df2) %>% 
  pull(level_ua)

levels_mun <- left_join(df, df2) %>% 
  pull(level_mun)

levels_ua <- levels_ua[-which(is.na(levels_mun))]
levels_mun <- levels_mun[-which(is.na(levels_mun))]
cor(levels_mun, levels_ua, method="kendall")

ranks_ua <- left_join(df, df2) %>% 
  pull(rank_ua)

ranks_mun <- left_join(df, df2) %>% 
  pull(rank_mun)

ranks_ua <- ranks_ua[-which(is.na(ranks_mun))]
ranks_mun <- ranks_mun[-which(is.na(ranks_mun))]
cor(ranks_mun, ranks_ua, method="kendall")


## <delta(h)> vs. h
left_join(df, df2) %>% 
  mutate(delta=abs(level_mun-level_ua)) %>% 
  group_by(level_ua) %>% 
  summarize(avg.delta=mean(delta, na.rm=T), 
            sd.delta=sd(delta, na.rm=T)) %>% 
  ggplot(aes(x=level_ua, y=avg.delta))+  
  geom_line()+
  geom_point(shape=21, fill="white")+
  scale_x_continuous(TeX("$h_{ua}$"))+
  scale_y_continuous(TeX("$<|h_{ua}-h_{mun}|>$"))+
  theme_bw()
ggsave("figs/FR_avg_delta_vs_hua.pdf", 
       width=3.4, 
       height=3.4, 
       units="in")

### h vs. rank for both definitions

df <- get_levels_p_phi(tree_ua) %>%
  ungroup() %>% 
  mutate(scale="urban area") %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n())

df2 <- get_levels_p_phi(tree_coremun) %>% 
  ungroup() %>% 
  mutate(scale="municipalities") %>% 
  arrange(desc(P)) %>% 
  mutate(rank=1:n())

rbind(df,df2) %>%
  filter(P>=1) %>%
  mutate(plotname=as.character(name), 
         level=as.integer(level),
         scale=as.factor(scale)) %>% 
  mutate(plotname=ifelse(level >=2,plotname,"")) %>% 
  ggplot(aes(x=P, y=level, size=phi, fill=scale))+
  geom_point(shape=21,alpha=.5,stroke=.2)+
  geom_text(aes(y=level-.4, label=plotname), size=1.5)+
  scale_x_log10()+
  scale_y_continuous("h")+#, breaks=0:5,labels=y_labels)+
  scale_size_area(name=TeX("$\\varphi$"), 
                  max_size = 8)+
  scale_fill_discrete(guide="none")+
  facet_wrap(~scale, ncol=1)+
  theme_bw()+
  theme(legend.position = c(1,1), 
        legend.justification = c(1.05,.86),
        legend.background = element_rect(color="black"))

ggsave("figs/FR_hi_vs_Pi_urban_areas_core_mun.pdf", 
       height=4, 
       width=6, 
       units="in")


